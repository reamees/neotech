package ee.com;

import ee.com.config.DBConfig;
import ee.com.exception.DBConnectionException;
import ee.com.service.DBService;
import ee.com.thread.TimestampConsumer;
import ee.com.thread.TimestampProducer;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class App
{
    private final LinkedBlockingQueue<Timestamp> queue = new LinkedBlockingQueue<>(DBConfig.SIZE);

    public static void main( String[] args )
    {
        if (args.length > 0 && args[0].trim().toLowerCase().equals("-p")) {
            try {
                new DBService().readTimestamps();
            } catch (DBConnectionException e) {
                System.out.println(e.getMessage());
            }
        } else {
            new App().startEngine();
        }
    }

    private void startEngine() {
        try {
            new DBService().createTable();
        } catch (DBConnectionException | SQLException e) {
            System.out.println(e.getMessage());
        }
        startTimestampProducer();
        startTimestampConsumer();
    }

    private void startTimestampProducer() {
        final TimestampProducer<Timestamp> timestampProducer = new TimestampProducer<>(queue);
        final Supplier<Timestamp> supplier = () -> new Timestamp(System.currentTimeMillis());
        new Thread(() -> {
            while (true) {
                timestampProducer.produce(supplier);
            }
        }).start();
    }

    private void startTimestampConsumer() {
        final TimestampConsumer<Timestamp> timestampConsumer = new TimestampConsumer<>(queue);
        final Consumer<Timestamp> consumer = (s)  ->  System.out.println("Saved : " + s.toString());
        new Thread(() -> {
            while (true)
                timestampConsumer.consume(consumer);
        }).start();
    }

}