package ee.com.config;

public class DBConfig {
    public static final String DBUSER = "root";
    public static final String DBPASSWD = "";
    public static final String URL = "jdbc:mysql://localhost:3306/neotech";
    public static final Integer SIZE = 10000;

}
