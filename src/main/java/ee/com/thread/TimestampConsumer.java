package ee.com.thread;

import ee.com.exception.DBConnectionException;
import ee.com.service.DBService;

import java.sql.Timestamp;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class TimestampConsumer<T extends Timestamp> {
    private LinkedBlockingQueue<T> queue;
    DBService dbService = new DBService();

    public TimestampConsumer(LinkedBlockingQueue<T> queue) {
        this.queue = queue;
    }

    public void consume(Consumer<T> consumer) {
        T time;
        while ((time = queue.peek()) != null) {
            try {
                dbService.saveTimestamp(time);
                queue.poll();
                consumer.accept(time);
            } catch (DBConnectionException e) {
                System.out.println(e.getMessage());
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
