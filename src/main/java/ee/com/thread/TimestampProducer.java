package ee.com.thread;

import java.sql.Timestamp;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class TimestampProducer<T extends Timestamp> {
    private LinkedBlockingQueue<T> queue;

    public TimestampProducer(LinkedBlockingQueue<T> queue) {
        this.queue = queue;
    }

    public void produce(Supplier<T> supplier) {
        final T msg = supplier.get();
        try {
            queue.put(msg);
            System.out.println("Added message: " + msg);
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}