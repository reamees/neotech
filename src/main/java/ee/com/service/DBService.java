package ee.com.service;
import ee.com.config.DBConfig;
import ee.com.exception.DBConnectionException;

import java.sql.*;

public class DBService {

    private Connection connection;

    public void saveTimestamp(Timestamp timestamp) throws DBConnectionException {
        connection = getConnection();
        try (PreparedStatement preparedStmt = connection.prepareStatement("insert into neo (time) values (?)")) {
            preparedStmt.setTimestamp(1, timestamp);
            preparedStmt.execute();
        } catch (SQLException ex) {
            closeConnection();
            throwDbDownException();
        }
    }

    public void readTimestamps() throws DBConnectionException {
        connection = getConnection();
        try (ResultSet rs = connection.createStatement().executeQuery("SELECT time FROM neo")) {
            while (rs.next()) {
                Timestamp timestamp = rs.getTimestamp("time");
                System.out.println("Timestamp from db : " + timestamp);
            }
        } catch (SQLException ex) {
            closeConnection();
            throwDbDownException();
        }
    }

    public void createTable() throws DBConnectionException, SQLException {
        connection = getConnection();
        DatabaseMetaData dbm = connection.getMetaData();
        ResultSet tables = dbm.getTables(null, null, "neo", null);
        if (!tables.next()) {
            String createTableSQL = "CREATE TABLE neo("
                    + "id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, "
                    + "time TIMESTAMP NOT NULL, "
                    + "PRIMARY KEY (id) "
                    + ")";
            PreparedStatement preparedStmt = connection.prepareStatement(createTableSQL);
            preparedStmt.execute();
        }
    }


    private Connection getConnection() throws DBConnectionException {
        if (connection == null)
            try {
                connection = DriverManager.getConnection(DBConfig.URL, DBConfig.DBUSER, DBConfig.DBPASSWD);
            } catch (SQLException ex) {
                connection = null;
                throwDbDownException();
            }
        return connection;
    }

    private void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                connection = null;
            }
        }
    }

    private void throwDbDownException() throws DBConnectionException {
        throw new DBConnectionException("DB -> Down");
    }

}
